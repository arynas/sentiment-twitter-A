# General
import tweepy
import pandas as pd

from IPython.display import display

# We import our access keys:
from credentials import *  # This will allow us to use the keys as variables


# API's setup:
def twitter_setup():
    """
    Utility function to setup the Twitter's API
    with our access keys provided.
    """
    # Authentication and access using keys:
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)

    # Return API with authentication:
    api = tweepy.API(auth)
    return api


# We create an extractor object:
extractor = twitter_setup()

# We create a tweet list as follows:
tweets = extractor.user_timeline(screen_name="realDonaldTrump", count=200)
# tweets = extractor.user_timeline(screen_name="laravelnews", count=200)

# print("Number of tweets extracted: {}.\n".format(len(tweets)))

data = pd.DataFrame(data=[tweet.text for tweet in tweets], columns=['Tweets'])

display(data)